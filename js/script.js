"use strict";

let arrowUp = document.querySelector("#arrowTop"),
    toTopInterval,
    supportedScrollTop;

arrowUp.addEventListener("click", function(){
    toTopInterval = setInterval(function(){
        supportedScrollTop = document.body.scrollTop > 0 ? document.body : document.documentElement;
            if (supportedScrollTop.scrollTop > 0) {
                supportedScrollTop.scrollTop = supportedScrollTop.scrollTop - 50;
            }
           if (supportedScrollTop.scrollTop < 1) {
                clearInterval(toTopInterval);
          }
       }, 10);
},false);

var myScrollFunc = function () {
  var y = window.scrollY;
  var navbar = document.getElementById("header");
  if (y >= 200) {
    navbar.classList.add("sticky");
      arrowUp.className = "show fa fa-arrow-up";
  } else {
      arrowUp.className = "hide";
      navbar.classList.remove("sticky");
  }
};

window.addEventListener("scroll", myScrollFunc);

